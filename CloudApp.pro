#-------------------------------------------------
#
# Project created by QtCreator 2015-04-10T08:21:39
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = CloudApp
CONFIG   += console
CONFIG   -= app_bundle
QT_INCLUDE_PATH = /usr/include/qt5/
LUA_INCLUDE_PATH = /usr/include/lua5.2
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH += $$QT_INCLUDE_PATH/QtNetwork\
               $$QT_INCLUDE_PATH/QtCore\
               $$LUA_INCLUDE_PATH\

SOURCES += main.cpp \
    host.cpp \
    Script.cpp \
    Server.cpp \
    Task.cpp

HEADERS += \
    Content.h \
    DataBase.h \
    host.h \
    hostsender.h \
    Logger.h \
    LuaExecutor.h \
    orgraph.h \
    Queue.h \
    RbTree.h \
    Script.h \
    Server.h \
    Task.h \
    TaskScheduler.h \
    ThreadPool.h \
    wrapper.h
